import logo from './logo.svg';
import './App.css';
import BaiTapThucHanhLayout from './r BaiTapLayoutComponent/BaiTapThucHanhLayout';

function App() {
  return (
    <div className="App">
      <BaiTapThucHanhLayout />
    </div>
  );
}

export default App;
